# linki
Fast Api:
- https://fastapi.tiangolo.com/
- https://fastapi.tiangolo.com/advanced/custom-response/#html-response
- https://fastapi.tiangolo.com/tutorial/response-model/#response-model
- https://fastapi.tiangolo.com/tutorial/response-status-code/

Typowanie zmiennych w modelach: 
- https://pydantic-docs.helpmanual.io/usage/models/

Typy:
- https://pydantic-docs.helpmanual.io/usage/types/

Funkcja do walidacji:
- https://pydantic-docs.helpmanual.io/usage/schema/#field-customisation
- https://pydantic-docs.helpmanual.io/usage/types/#constrained-types

##### Software
- https://www.postman.com/

##### Zasady projektowe
- https://devcave.pl/notatnik-juniora/zasady-projektowania-kodu

from typing import List

from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session

import crud, models, schemas
from database import SessionLocal, engine, get_db

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

# Path
@app.get("/results/", response_model=List[schemas.Tuple])
def read_results(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    query = crud.get_results(db, skip=skip, limit=limit)
    return query


@app.post("/calc/{operation}", response_model=schemas.Tuple)
def calculator(data: schemas.UserIn, db: Session = Depends(get_db)):
    return crud.create_result(db=db, data=data)

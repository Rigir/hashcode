from pydantic import BaseModel, validator, ValidationError
from typing import List

class UserIn(BaseModel):
    operation: str = '+'
    x: float = 0
    y: float = 0
    
    @validator('operation')
    def operator_doesnt_exist(cls, v):
        operations = ['+', '-', '/', '//', '*', '**', '%']
        if v not in operations:
            raise ValueError('Operator does not exist')
        return v
    
class Tuple(BaseModel):
    id: int
    result: int = 0
    
    class Config:
        orm_mode = True

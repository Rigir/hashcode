from sqlalchemy.orm import Session
import models, schemas


def get_results(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Calc).offset(skip).limit(limit).all()


def create_result(db: Session, data: schemas.UserIn):
    query = models.Calc(result=float(
        eval(f' {data.x} {data.operation} {data.y}')))
    db.add(query)
    db.commit()
    db.refresh(query)
    return query

from sqlalchemy import Column, Integer, Float
from database import Base


class Calc(Base):
    __tablename__ = 'math_results'

    id = Column(Integer, primary_key=True, index=True)
    result = Column(Float)

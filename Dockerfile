FROM python

RUN mkdir -p /home/app
WORKDIR /home/app

COPY requirements.txt . 
RUN pip install --no-cache-dir -r requirements.txt

COPY . . 

ENTRYPOINT ["python"]